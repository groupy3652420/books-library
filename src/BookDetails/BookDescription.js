import React from "react";
import { useOutletContext } from "react-router-dom";

const BookDescription = () => {
  const { book } = useOutletContext();
  return (
    <div>
      <h2 className="heading">Description</h2>
      <p>{book?.description}</p>
    </div>
  );
};

export default BookDescription;
