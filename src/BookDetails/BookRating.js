import React from "react";
import { useOutletContext } from "react-router-dom";
const BookRating = () => {
  const { book } = useOutletContext();
  return (
    <div>
      <h2 className="heading">Rating</h2>
      <p>{book?.rating}</p>
    </div>
  );
};

export default BookRating;
