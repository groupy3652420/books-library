import React from "react";
import { useOutletContext } from "react-router-dom";
const BookQuotes = () => {
  const { book } = useOutletContext();
  return (
    <div>
      <h2 className="heading">Quotes</h2>
      <p>{book?.Quote1}</p>
      <p>{book?.Quote2}</p>
      <p>{book?.Quote3}</p>
    </div>
  );
};

export default BookQuotes;
