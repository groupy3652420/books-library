import { useState, useEffect } from "react";
import axios from "axios";
import React from "react";
import { NavLink, Outlet, useParams } from "react-router-dom";
import { BOOK_DETAILS_URL } from "../API";
import "./bookDetails.scss";
const BookDetails = () => {
  const [book, setBook] = useState({});
  const { id } = useParams();
  const activeStyles = {
    fontWeight: "bold",
    color: "#970621",
  };

  useEffect(() => {
    axios
      .get(`${BOOK_DETAILS_URL}/${id}`)
      .then((res) => setBook(res.data))
      .catch((err) => console.log(err));
  }, [id]);
  return (
    <div className="book-details">
      <div className="menu">
        <div className="menu-item">
          <NavLink
            to="."
            end
            style={({ isActive }) => (isActive ? activeStyles : null)}
          >
            Description
          </NavLink>
        </div>
        <div className="menu-item">
          <NavLink
            to="authors"
            style={({ isActive }) => (isActive ? activeStyles : null)}
          >
            Authors
          </NavLink>
        </div>
        <div className="menu-item">
          <NavLink
            to="rating"
            style={({ isActive }) => (isActive ? activeStyles : null)}
          >
            Rating
          </NavLink>
        </div>
        <div className="menu-item">
          <NavLink
            to="quotes"
            style={({ isActive }) => (isActive ? activeStyles : null)}
          >
            Quotes
          </NavLink>
        </div>
      </div>
      <div className="book-img">
        <img src={book?.image_url} alt="" />
      </div>
      <div className="book-description">
        <h1 className="title">{book?.title}</h1>
        <Outlet context={{ book }} />
      </div>
    </div>
  );
};

export default BookDetails;
