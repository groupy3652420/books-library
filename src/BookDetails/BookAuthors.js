import React from "react";
import { useOutletContext } from "react-router-dom";
const BookAuthors = () => {
  const { book } = useOutletContext();
  return (
    <div>
      <h2 className="heading">Authors</h2>
      <p>{book?.authors}</p>
    </div>
  );
};

export default BookAuthors;
