import { Route, Routes } from "react-router-dom";
import BooksList from "./BookList/BooksList";
import Favorites from "./FavoritesBooks/Favorites";
import BookDetails from "./BookDetails/BookDetails";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import BookDescription from "./BookDetails/BookDescription";
import BookAuthors from "./BookDetails/BookAuthors";
import BookRating from "./BookDetails/BookRating";
import BookQuotes from "./BookDetails/BookQuotes";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" exact element={<BooksList />} />
        <Route path="/favorites" element={<Favorites />} />
        <Route path="/books/:id" element={<BookDetails />}>
          <Route index element={<BookDescription />} />
          <Route path="authors" element={<BookAuthors />} />
          <Route path="rating" element={<BookRating />} />
          <Route path="quotes" element={<BookQuotes />} />
        </Route>
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
