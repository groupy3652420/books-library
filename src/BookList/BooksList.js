import React from "react";
import { useState, useEffect } from "react";
import { API_URL } from "../API";
import axios from "axios";
import { useAppContext } from "../context/appContext";
import { useNavigate } from "react-router-dom";
import "./bookList.scss";
const BooksList = () => {
  const [books, setBooks] = useState([]);
  const { favorites, addToFavorites, removeFromFavorites } = useAppContext();
  const navigate = useNavigate();
  const favoriteToggler = (id) => {
    const boolean = favorites.some((book) => book.id === id);
    return boolean;
  };
  useEffect(() => {
    axios
      .get(API_URL)
      .then((res) => setBooks(res.data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="books-list">
      {books.map((book) => (
        <div key={book.id} className="book">
          <div>
            <img
              src={book.image_url}
              alt="#"
              onClick={() => navigate(`/books/${book.id}`)}
            />
          </div>
          <div className="div-btn">
            {favoriteToggler(book.id) ? (
              <button
                onClick={() => {
                  removeFromFavorites(book.id);
                }}
              >
                Remove from favorites
              </button>
            ) : (
              <button
                onClick={() => {
                  addToFavorites(book);
                }}
              >
                Add to favorites
              </button>
            )}
          </div>
        </div>
      ))}
    </div>
  );
};

export default BooksList;
