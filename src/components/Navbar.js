import React from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookOpenReader } from "@fortawesome/free-solid-svg-icons";
import "./navfo.scss";

const Navbar = () => {
  return (
    <div className="navbar">
      <div className="logo-div">
        <FontAwesomeIcon icon={faBookOpenReader} className="logo-icon" />
        <NavLink to="/">
          {" "}
          <h1 className="logo">Books App</h1>
        </NavLink>
      </div>
      <NavLink to="/favorites">
        <div className="fav-link">my Favorite</div>
      </NavLink>
    </div>
  );
};

export default Navbar;
