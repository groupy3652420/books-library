import React from "react";
import { useNavigate } from "react-router-dom";
import { useAppContext } from "../context/appContext";
import "./favorites.scss";
const Favorites = () => {
  const { favorites, removeFromFavorites } = useAppContext();
  const navigate = useNavigate();
  return (
    <>
      {favorites.length > 0 ? (
        <div className="favorites">
          {favorites.map((book) => (
            <div key={book.id} className="book">
              <div>
                <img
                  onClick={() => {
                    navigate(`/books/${book.id}`);
                  }}
                  src={book.image_url}
                  alt="#"
                />
              </div>
              <div>
                <button
                  onClick={() => {
                    removeFromFavorites(book.id);
                  }}
                >
                  Remove from favorites
                </button>
              </div>
            </div>
          ))}
        </div>
      ) : (
        <div className="container">
          <div>
            {" "}
            <h2 className="no-fav">you don't have any favorites books yet</h2>
          </div>
          <div className="return-btn">
            <button onClick={() => navigate("/")}>return to home</button>
          </div>
        </div>
      )}
    </>
  );
};

export default Favorites;
